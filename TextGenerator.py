from enum import Enum
import random


class Wordtype(Enum):
    REGULAR = 0     # Regular word
    FIRST = 1       # First of a sentence (uppercase)
    LAST = 2        # Last of a sentence (period)
    NAN = 3         # Not an actual word, reserved for the base on


class TextGenerator:

    # Properties

    words = None

    # Functions

    # ----------------------------------------
    def __init__(self, filename):

        self.words = Word()
        self.words.type = Wordtype.NAN

        self.build_from_file(filename)

    # ----------------------------------------
    def build_from_file(self, filename):

        with open(filename, 'r') as file:

            source = file.read().replace('\n', ' ')

        # Explode string to words
        source_words = source.split()

        # Build bi-grams

        # 99 bottles of beer --->
        # 99 => [ bottles => [ of ... ] ]

        for i, word in enumerate(source_words):

            if i < 2:
                continue

            word_1 = source_words[i - 2]
            word_2 = source_words[i - 1]
            word_3 = source_words[i]

            # Create elements
            if word_1 not in self.words.children:
                self.words.children[word_1] = Word()

            if word_2 not in self.words.children[word_1].children:
                self.words.children[word_1].children[word_2] = Word()

            if word_3 not in self.words.children[word_1].children[word_2].children:
                self.words.children[word_1].children[word_2].children[word_3] = Word()

            # Set type in sentence
            if i == 2 or source_words[i - 3].endswith("."):
                self.words.children[word_1].type = Wordtype.FIRST

            if word_1.endswith("."):
                self.words.children[word_1].type = Wordtype.LAST
                self.words.children[word_1].children[word_2].type = Wordtype.FIRST

            if word_2.endswith("."):
                self.words.children[word_1].children[word_2].type = Wordtype.LAST
                self.words.children[word_1].children[word_2].children[word_3].type = Wordtype.FIRST

            if word_3.endswith("."):
                self.words.children[word_1].children[word_2].children[word_3].type = Wordtype.LAST

            # Set counts
            self.words.children[word_1].count += 1
            self.words.children[word_1].children[word_2].count += 1
            self.words.children[word_1].children[word_2].children[word_3].count += 1

    # ----------------------------------------
    def random_child(self, children):

        weights = []
        words = []

        for key, child in children.items():
            words.append(key)
            weights.append(child.count)

        return random.choices(words, weights, k=1)[0]

    # ----------------------------------------
    def start_sentence(self):

        # Get only the starter words
        starter_words = {word: o for word, o in self.words.children.items() if o.type == Wordtype.FIRST}

        return self.random_child(starter_words)

    # ----------------------------------------
    def get_second_word(self, sentence):

        word_1 = sentence[-1]
        return self.random_child(self.words.children[word_1].children)

    # ----------------------------------------
    def get_third_word(self, sentence):

        word_1 = sentence[-2]
        word_2 = sentence[-1]
        return self.random_child(self.words.children[word_1].children[word_2].children)

    # ----------------------------------------
    def generate_sentence(self, sentence):

        if len(sentence) > 1:
            return self.get_third_word(sentence)

        return self.get_second_word(sentence)

    # ----------------------------------------
    def build_sentence(self):

        sentence = [self.start_sentence()]

        while not sentence[-1].endswith('.'):
            sentence.append(self.generate_sentence(sentence))

        return ' '.join(sentence)

    # ----------------------------------------
    def generate(self, n):

        text = ""

        for i in range(n):
            text += self.build_sentence() + " "

        return text.strip()


class Word:

    # Properties

    children = {}
    type = Wordtype.REGULAR
    count = 0

    # Functions

    def __init__(self):
        self.children = {}
        self.type = Wordtype.REGULAR
        self.count = 0

    def __repr__(self):
        return "<Word count:{}, children:{}, type:{}>".format(self.count, len(self.children), self.type.name)

    def child(self, key):
        return self.children[key]

    def create_child(self, key):
        self.children[key] = Word

    def increment_all(self):

        self.count += 1

        if self.children:
            for key, child in self.children.items():
                child.increment_all()

    def has_child(self, key):
        return key in self.children
