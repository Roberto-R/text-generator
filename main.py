from TextGenerator import TextGenerator
import sys

settings = sys.argv[1:]

if len(settings) != 2:
    raise SystemError("Two arguments need to be passed: the source file and the number of output sentences")

file = settings[0]
n = int(settings[1])

generator = TextGenerator(file)

print(generator.generate(n))
